# Welcome to the NextCloudInstaller wiki!

## Quick Start 

1. Ensure you are running Ubuntu 16.04 64bit and your domain is setup to the machine/vps.
2. SSH into the server and ensure you are the root user.
3. `curl -fsSL https://raw.githubusercontent.com/desgyz/NextCloudInstaller/master/installer.bash -o installer.bash`
3. `bash installer.bash`
4. Once the machine/vps has rebooted. Head to `https://<yourdomain>`
5. Done

**Notes**
* Only install one version of PHP. Caddy, NextCloud, MySQL and PHP are required.
* CODE is currently broken.
* Recommended NextCloud data directory is `/mnt/NextCloud/Data`.
* The Database name is `nextcloud` and the username is `root`.
